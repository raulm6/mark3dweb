'use strict';

const express = require('express')
const app = express()
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require ('path');

// configuration
app.engine('handlebars', exphbs({
  defaultLayout: 'layout',
}));


app.use(express.static(path.join(__dirname + '/public')));
app.use(express.static(path.join(__dirname)));
app.set('views', path.join(__dirname + '/views'));
app.set('view engine', 'handlebars');
app.use(bodyParser.urlencoded({
  extended: true,
})); // for parsing application/x-www-form-urlencoded

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
  res.render('index');

})

app.get('/wholesiteVR', function (req, res) {
  res.render('wholesite');

})

app.get('/blend', function (req, res) {
  res.render('blend');

})

app.get('/demo1', function (req, res) {
  res.render('demo1');

})

app.get('/demo2', function (req, res) {
  res.render('demo2');

})

app.get('/demo3', function (req, res) {
  res.render('demo3');

})

app.get('/demo4', function (req, res) {
  res.render('demo4');

})

app.get('/demo5', function (req, res) {
  res.render('demo5');

})

app.get('/demos', function (req, res) {
  res.render('demos');

})

app.get('/appVR', function (req, res) {
  res.render('appVR');

})

app.get('/app', function (req, res) {
  res.render('app');

})

app.get('/about', function (req, res) {
  res.render('about');

})

app.get('/contact', function (req, res) {
  res.render('contact');

})

// use port 3000 unless there exists a preconfigured port
var port = process.env.PORT || 3000;

app.listen(port, function () {
  console.log('Mark3DWeb listening on port ' + port);
})
