// Last time the scene was rendered.
var lastRenderTime = 0;
// Currently active VRDisplay.
var vrDisplay;
// How big of a box to render.
var boxSize = 5;
// Various global THREE.Objects.
var scene;
var cube;
//var VRcontrols;
var effect;
//var VRcamera;
var loadedobj
// EnterVRButton for rendering enter/exit UI.
var vrButton;

var inVR = false;


/* GLOBAL VARS */
var DEBUG
var initialPositions = {x: 0, y: 0, z: 0};
var initialLookAt = new THREE.Vector3( 0, 0, 10 );
var up = new THREE.Vector3( 0, 1, 0 );
var Xaxis = new THREE.Vector3(1,0,0);
var Yaxis = new THREE.Vector3(0,1,0);
var Zaxis = new THREE.Vector3(0,0,1);

// WebGL DOM Element

var CANVAS_WIDTH = window.innerWidth;
var CANVAS_HEIGHT = window.innerHeight;
//var container = document.getElementById('WebGL-output');


// ThreeJS Rendering
var currentCamera;
var scene;
var renderer;
var ASPECTRATIO = .75;

// UI
var gui, stats, datControls, orbitControls, dragControls, axisHelper, gridHelper, stage;
var moveableLights = [];
var availableCameras = [];

// Object Seelction
var mouse = new THREE.Vector2();
var cursorPosition = [];

// Ray Casting
var raycaster = new THREE.Raycaster();
var INTERSECTED;
var INTERSECTEDPROPS;
var clickPoint = null;
var intersectedType = null;
// objects checked by raycaster
var objects = [];
// Currently selected object
var selectedObject = null;

// Decals
// Currently Selected Decal
var selectedDecal = null;
var mouseHelper;
var intersection = {
			intersects: false,
			point: new THREE.Vector3(),
			normal: new THREE.Vector3()
};
var decalMaterial;
var decalDiffuse;
var decalNormal;
var line, lineGeometry;
var decalPosition = new THREE.Vector3();
var decalOrientation = new THREE.Euler();
var decalSize = new THREE.Vector3( 3, 3, 3 );


// CAMERAS
var camera;
var VRcamera;
var firstimeVR = true;

// CONTROLS
var fpVrControls;
var VRcontrols;
var TransformControls;

// MODELS
var massivedoor;
var roofdetail;
var wholesite;
var bimaristantext;
var rooftext;



var DEBUG = false;
if (DEBUG) { console.log('---- DEBUG = true') };

function onLoad() {

	// TEXTURE LOADING
	var textureLoader = new THREE.TextureLoader();
	var textureMark3D = textureLoader.load( "textures/Mark3D.png" );

	stats = initStats();
  // Setup three.js WebGL renderer. Note: Antialiasing is a big performance hit.
  // Only enable it if you actually need to.
  var renderer = new THREE.WebGLRenderer({antialias: true});
  renderer.setPixelRatio(window.devicePixelRatio);

  // Append the canvas element created by the renderer to document body element.
  document.body.appendChild(renderer.domElement);

  // Create a three.js scene.
  scene = new THREE.Scene();

  // Create a three.js camera.
  var aspect = window.innerWidth / window.innerHeight;
  VRcamera = new THREE.PerspectiveCamera(75, aspect, 0.1, 100000000);
	VRcamera.position.y = 10;
	VRcamera.position.x = 20;
	VRcamera.position.z = 20;

  scene.add( new THREE.AmbientLight( 0x505050 ) )
	availableCameras.push(VRcamera);
  var spotLight = new THREE.SpotLight(0xffffff);
  spotLight.position.set(-40, 60, -10);
  spotLight.castShadow = true;
  spotLight.shadow.mapSize.height = 2048;
  spotLight.shadow.mapSize.width = 2048;
  scene.add(spotLight);

  VRcontrols = new THREE.VRControls(VRcamera);
  VRcontrols.standing = true;
	VRcontrols.movementSpeed = 3.0;

	fpVrControls = new THREE.FirstPersonVRControls(VRcamera, scene);
// Optionally enable vertical movement.
	fpVrControls.verticalMovement = true;

  // Apply VR stereo rendering to renderer.
  effect = new THREE.VREffect(renderer);
  effect.setSize(window.innerWidth, window.innerHeight);

  /* Create 3D objects.
  objLoader = new THREE.OBJLoader();

  objLoader.load(
      'models/monkey.obj',
      function(obj) {
          //add the loaded object to the scene
          obj.position.set(0, VRcontrols.userHeight, -1);
          obj.scale.set(0.1,0.1,0.1);

          // CREATE THE INITIAL COLORS
           obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});


					for (var i = 0; i < obj.children.length; i++) {
						obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});

						cube = obj.children[i];
						obj.children[i].decals = [];
						obj.children[i].decalinfo =[]


						scene.add(obj.children[i])
						objects.push(cube);



					}

          //scene.add(obj)
					//objects.push(obj);

          if (DEBUG) {
						//console.log(obj);
          	console.log('--- LOADED MONKEY DEBUG ---');
					}


      },

      // Function called when download progresses
      function ( xhr ) {
        console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
      },

      // Function called when download errors
      function ( xhr ) {
        console.error( 'An error happened while loading an .OBJ Model from: ' + path  );
      }
  );
*/

	// instantiate a loader
	var loader = new THREE.JSONLoader();

	// load a resource
	loader.load(

		// resource URL
		'models/JSON/massivedoor_light.json',

		// Function when resource is loaded
		function ( geometry, materials ) {

			var loader = new THREE.FontLoader();

			loader.load( 'fonts/helvetiker_regular.typeface.json', function ( font ) {

				var textgeometry = new THREE.TextGeometry( 'Bimaristan Nur al-Din', {
					font: font,
					size: 0.5,
					height: 1,
					curveSegments: 1,
					bevelEnabled: false,
					bevelThickness: 1,
					bevelSize: 1,
					bevelSegments: 1
				} );

				var material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});

				bimaristantext = new THREE.Mesh( textgeometry, material );
				bimaristantext.position.set(5, 0, 5);
				bimaristantext.rotateY(Math.PI - .141516);
				bimaristantext.visible = false;

				scene.add( bimaristantext );


			} );



			var material = materials[0];//new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,}); //materials[ 0 ];
			var object = new THREE.Mesh( geometry, material );
			object.position.set(0, -1.6, 5);

			scene.add( object );
			massivedoor = object;
			Reticulum.add( massivedoor, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				bimaristantext.visible = true;
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				bimaristantext.visible = false;
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );

				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				this.material.emissive.setHex( 0x00cccc );
				}
			});

			var geometry = new THREE.SphereGeometry( 0.5, 32, 32 );
			var material = new THREE.MeshLambertMaterial({color: 0xcc11ff, emissive : 0x000000,});
			var sphere = new THREE.Mesh( geometry, material );
			sphere.position.set(0, 0, -5);
			scene.add( sphere );

			Reticulum.add( sphere, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
						fpVrControls.object.position.set(0, 0, -5)
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				//this.material.emissive.setHex( 0x00cccc );
				fpVrControls.object.position.set(0, 0, -5)

				}
			});
	var material = new THREE.MeshLambertMaterial({color: 0xcc11ff, emissive : 0x000000,});
			var sphere = new THREE.Mesh( geometry, material );
			sphere.position.set(0, 3, -10);
			scene.add( sphere );

			Reticulum.add( sphere, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
					fpVrControls.object.position.set(0, 3, -10);
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				//this.material.emissive.setHex( 0x00cccc );
				fpVrControls.object.position.set(0, 3, -10);

				}
			});

	var material = new THREE.MeshLambertMaterial({color: 0xcc11ff, emissive : 0x000000,});
			var sphere = new THREE.Mesh( geometry, material );
			sphere.position.set(10, 7, 13);
			scene.add( sphere );

			Reticulum.add( sphere, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
								fpVrControls.object.position.set(10, 7, 13);
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				//this.material.emissive.setHex( 0x00cccc );
				fpVrControls.object.position.set(10, 7, 13);

				}
			});

	var material = new THREE.MeshLambertMaterial({color: 0xcc11ff, emissive : 0x000000,});
			var sphere = new THREE.Mesh( geometry, material );
			sphere.position.set(27, 30, 10);
			scene.add( sphere );

			Reticulum.add( sphere, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
				fpVrControls.object.position.set(27, 30, 10);
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				//this.material.emissive.setHex( 0x00cccc );
				fpVrControls.object.position.set(27, 30, 10);

				}
			});


		}
	);

	// load a resource
	loader.load(

		// resource URL
		'models/JSON/roofdetail_light.json',

		// Function when resource is loaded
		function ( geometry, materials ) {

			var material = materials[0];//new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,}); //materials[ 0 ];
			var object = new THREE.Mesh( geometry, material );
			object.position.set(25, 2.5, -5);

			var geometry = new THREE.SphereGeometry( 0.5, 32, 32 );
			var material = new THREE.MeshLambertMaterial({color: 0xcc11ff, emissive : 0x000000,});
			var sphere = new THREE.Mesh( geometry, material );
			sphere.position.set(25, -0.5, -5);
			scene.add( sphere );


			var loader = new THREE.FontLoader();

			loader.load( 'fonts/helvetiker_regular.typeface.json', function ( font ) {

				var textgeometry = new THREE.TextGeometry( 'Roof Interior', {
					font: font,
					size: 0.5,
					height: 0.5,
					curveSegments: 1,
					bevelEnabled: false,
					bevelThickness: 1,
					bevelSize: 1,
					bevelSegments: 1
				} );

				var material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});

				rooftext = new THREE.Mesh( textgeometry, material );
				rooftext.position.set(23, 2.0, -5);
				//rooftext.rotateY(Math.PI);
				rooftext.rotateX(Math.PI/2);
				rooftext.visible = false;

				scene.add( rooftext );


			} );


			Reticulum.add( sphere, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );

				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );

				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
				fpVrControls.object.position.set(25, 0, -5)
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				//this.material.emissive.setHex( 0x00cccc );
				fpVrControls.object.position.set(25, 0, -5)

				}
			});



			scene.add( object );
			roofdetail_light = object;
			Reticulum.add( roofdetail_light, {
				clickCancelFuse: true, // Overrides global setting for fuse's clickCancelFuse
				reticleHoverColor: 0x00fff6, // Overrides global reticle hover color
				fuseVisible: true, // Overrides global fuse visibility
				fuseDuration: 1.5, // Overrides global fuse duration
				fuseColor: 0xcc0000, // Overrides global fuse color
				onGazeOver: function(){
				// do something when user targets object
				this.material.emissive.setHex( 0xffcc00 );
				rooftext.visible = true;
				},
				onGazeOut: function(){
				// do something when user moves reticle off targeted object
				this.material.emissive.setHex( 0xcc0000 );
				rooftext.visible = false;
				},
				onGazeLong: function(){
				// do something user targetes object for specific time
				this.material.emissive.setHex( 0x0000cc );
				},
				onGazeClick: function(){
				// have the object react when user clicks / taps on targeted object
				this.material.emissive.setHex( 0x00cccc );
				}
			});
		}
	);


	loader.load(

		// resource URL
		'models/JSON/wholesite_light.json',

		// Function when resource is loaded
		function ( geometry, materials ) {

			var bufferGeo = new THREE.BufferGeometry();
			bufferGeo.fromGeometry(geometry);
			var material = materials[0];//new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,}); //materials[ 0 ];
			var object = new THREE.Mesh( bufferGeo, material );
			object.position.set(0, -1.6, 0);

			scene.add( object );
			document.getElementById('loading').style.display = 'none';
			onTextureLoaded();


		}
	);



	// Decal Mouse Helper
	mouseHelper = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 5 ), new THREE.MeshNormalMaterial() );
	mouseHelper.visible = false;
	scene.add( mouseHelper );

	// Decals Line Cursor
	lineGeometry = new THREE.Geometry();
	lineGeometry.vertices.push( new THREE.Vector3(), new THREE.Vector3() );
	line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { linewidth: 4 } ) );
	line.visible = false;
	scene.add( line );

	/* UI Guide AXIS
	axisHelper = new THREE.AxisHelper( 60 );
	axisHelper.tag = 'axes';
	axisHelper.position.y = 0.005;
	//axisHelper.matrixAutoUpdate  = false;
	scene.add( axisHelper );
*/
	// UI Helper Grid
	var size = 50;
	var divisions = 50;
	gridHelper = new THREE.GridHelper( size, divisions );
	gridHelper.tag = 'grid';
	gridHelper.position.set(0, -1.6, 0);
	gridHelper.matrixAutoUpdate = true;
	//scene.add( gridHelper );

/*
	// Scene Stage
	var planeGeometry = new THREE.PlaneGeometry(50, 50, 1, 1);
	var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff, map: textureMark3D});
	planeMaterial.side = THREE.DoubleSide;

	stage = new THREE.Mesh(planeGeometry, planeMaterial);
	stage.receiveShadow = true;
	stage.tag = 'stage';
	stage.visible = false;
	// rotate and position the plane
	stage.rotation.x = -0.5 * Math.PI;

	scene.add(stage);
*/

	var sky, sunSphere;


	initSky();
	/* LIGHTS */
	// ambient lighting
	scene.add( new THREE.AmbientLight( 0x505050 ) )

	// add spotlight for the shadows
	var spotLight = new THREE.SpotLight(0xffffff);
	spotLight.position.set(-400, 600, -100);
	spotLight.castShadow = true;
	spotLight.shadow.mapSize.height = 2048;
	spotLight.shadow.mapSize.width = 2048;
	//scene.add(spotLight);
	moveableLights.push(spotLight);

	/* CAMERAS */
	camera = new THREE.PerspectiveCamera(45, CANVAS_WIDTH / CANVAS_HEIGHT, 0.5,2000000);// 1000);
	// Position and point the camera to the center of the scene
	camera.position.x = 20;
	camera.position.y = 20;
	camera.position.z = 20;
	camera.lookAt(scene.position);
	availableCameras.push(camera);
	currentCamera = camera;

	/* Resize Listeners */
  window.addEventListener('resize', onResize, true);
  window.addEventListener('vrdisplaypresentchange', onResize, true);


	/* OrbitControls - PAN, ZOOM, ORBIT */
	orbitControls = new THREE.OrbitControls(currentCamera, renderer.domElement);
	orbitControls.enabled = true;

	/*  DragControls  - Drag Objects */
	dragControls = new THREE.DragControls(objects, currentCamera, renderer.domElement );
	dragControls.enabled = false;

	/* TRansform Controls */
	TransformControls = new THREE.TransformControls( camera, renderer.domElement );
	//TransformControls.addEventListener( 'change', animate );

	scene.add( TransformControls );




	dragControls.addEventListener('dragstart',
		function (event) {
			if (dragControls.enabled) {
				orbitControls.enabled = false;
			}
		}
	);

	dragControls.addEventListener('dragend',
		 function (event) {
			if (dragControls.enabled) {
				orbitControls.enabled = true;
			}
		}
	);


	var bE = function () {

      return new Promise(function (resolve) {

        return resolve();
      });
    }


  // Initialize the WebVR UI.
  var uiOptions = {
    color: 'black',
    background: 'white',
    corners: 'square',
		beforeEnter: bE
  };
  vrButton = new webvrui.EnterVRButton(renderer.domElement, uiOptions);
  vrButton.on('exit', function() {
    currentCamera.quaternion.set(0, 0, 0, 1);
    //currentCamera.position.set(0, VRcontrols.userHeight, 0);
  datControls.currentCamera = 'orbit';
	document.getElementById('blackbar').style.display = 'inherit';
	document.getElementById('controls-div-div2').style.display = 'inherit';
		console.log('exiting VR')
  });
	vrButton.on('enter', function() {
		console.log('entering VR')
		datControls.currentCamera = 'fp';
		document.getElementById('blackbar').style.display = 'none';
		document.getElementById('controls-div-div2').style.display = 'none';
    //currentCamera.quaternion.set(0, 0, 0, 1);
    //currentCamera.position.set(0, VRcontrols.userHeight, 100);
  });
  vrButton.on('hide', function() {
    document.getElementById('ui').style.display = 'none';
  });
  vrButton.on('show', function() {
    document.getElementById('ui').style.display = 'inherit';
  });
  document.getElementById('vr-button').appendChild(vrButton.domElement);
  document.getElementById('magic-window').addEventListener('click', function() {
		datControls.currentCamera = 'fp';
    vrButton.requestEnterFullscreen();


  });

	


	// VR Click
	renderer.domElement.addEventListener( "touchstart", onCardboardTouch );
	renderer.domElement.addEventListener( "click", onCardboardTouch);

	Reticulum.init(VRcamera, {
	proximity: false,
	clickevents: true,
	reticle: {
		visible: true,
		restPoint: 5, //Defines the reticle's resting point when no object has been targeted
		color: 0xcc00cc,
		innerRadius: 0.0001,
		outerRadius: 0.003,
		hover: {
			color: 0x00cccc,
			innerRadius: 0.02,
			outerRadius: 0.024,
			speed: 5,
			vibrate: 50 //Set to 0 or [] to disable
		}
	},
	fuse: {
		visible: false,
		duration: 4.0,
		color: 0x00fff6,
		innerRadius: 0.045,
		outerRadius: 0.06,
		vibrate: 0, //Set to 0 or [] to disable
		clickCancelFuse: false //If users clicks on targeted object fuse is canceled
	}
});


scene.add(VRcamera)

}

function onCardboardTouch() {
		//massivedoor.position.y += 1;

	  // fpVrControls.object.position.y += 10;
		console.log(fpVrControls);
		console.log('click! gg')
}

function initSky() {
				// Add Sky Mesh
				sky = new THREE.Sky();
				scene.add( sky.mesh );
				// Add Sun Helper
				sunSphere = new THREE.Mesh(
					new THREE.SphereBufferGeometry( 20000, 16, 8 ),
					new THREE.MeshBasicMaterial( { color: 0xffffff } )
				);
				sunSphere.position.y = - 700000;
				sunSphere.visible = false;
				scene.add( sunSphere );
				/// GUI
				var effectController  = {
					turbidity: 10,
					rayleigh: 2,
					mieCoefficient: 0.005,
					mieDirectionalG: 0.8,
					luminance: 1,
					inclination: 0.49, // elevation / inclination
					azimuth: 0.25, // Facing front,
					sun: true
				};
				var distance = 400000;
				function guiChanged() {
					var uniforms = sky.uniforms;
					uniforms.turbidity.value = effectController.turbidity;
					uniforms.rayleigh.value = effectController.rayleigh;
					uniforms.luminance.value = effectController.luminance;
					uniforms.mieCoefficient.value = effectController.mieCoefficient;
					uniforms.mieDirectionalG.value = effectController.mieDirectionalG;
					var theta = Math.PI * ( effectController.inclination - 0.5 );
					var phi = 2 * Math.PI * ( effectController.azimuth - 0.5 );
					sunSphere.position.x = distance * Math.cos( phi );
					sunSphere.position.y = distance * Math.sin( phi ) * Math.sin( theta );
					sunSphere.position.z = distance * Math.sin( phi ) * Math.cos( theta );
					sunSphere.visible = effectController.sun;
					sky.uniforms.sunPosition.value.copy( sunSphere.position );

				}
				var guitwo = new dat.GUI();
				guitwo.add( effectController, "turbidity", 1.0, 20.0, 0.1 ).onChange( guiChanged );
				guitwo.add( effectController, "rayleigh", 0.0, 4, 0.001 ).onChange( guiChanged );
				guitwo.add( effectController, "mieCoefficient", 0.0, 0.1, 0.001 ).onChange( guiChanged );
				guitwo.add( effectController, "mieDirectionalG", 0.0, 1, 0.001 ).onChange( guiChanged );
				guitwo.add( effectController, "luminance", 0.0, 2 ).onChange( guiChanged );
				guitwo.add( effectController, "inclination", 0, 1, 0.0001 ).onChange( guiChanged );
				guitwo.add( effectController, "azimuth", 0, 1, 0.0001 ).onChange( guiChanged );
				guitwo.add( effectController, "sun" ).onChange( guiChanged );
				document.getElementById('controls-div-div2').appendChild(guitwo.domElement);
				guiChanged();
}

function onTextureLoaded(texture) {
  //texture.wrapS = THREE.RepeatWrapping;
  //texture.wrapT = THREE.RepeatWrapping;
  //texture.repeat.set(boxSize, boxSize);

  //var geometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
  //var material = new THREE.MeshBasicMaterial({
//    map: texture,
//    color: 0x01BE00,
//    side: THREE.BackSide
//  });

  // Align the skybox to the floor (which is at y=0).
  //skybox = new THREE.Mesh(geometry, material);
  //skybox.position.y = boxSize/2;
  //scene.add(skybox);

  // For high end VR devices like Vive and Oculus, take into account the stage
  // parameters provided.
  setupStage();
}



// Request animation frame loop function
function animate(timestamp) {
	//console.log(VRcamera.position)
  var delta = Math.min(timestamp - lastRenderTime, 500);
  lastRenderTime = timestamp;
  // Apply rotation to cube mesh
  //cube.rotation.y += delta * 0.0006;

	/* UPDATES */
  // Only update controls if we're presenting.
  if (vrButton.isPresenting() || dat.currentCamera == '') {
    VRcontrols.update();
		fpVrControls.update(timestamp);
  }

	datControls.updateFields(gui, true);
	stats.update();
	orbitControls.update();
	TransformControls.update();

  // Render the scene.

	switch (datControls.currentCamera) {
			case 'orbit':
					currentCamera = camera;

					break;
			case 'fp':

					currentCamera = VRcamera;

					break;
			default:
					currentCamera = camera;

					break;
	 }

	Reticulum.update();
  effect.render(scene, currentCamera);

  vrDisplay.requestAnimationFrame(animate);
}

function onResize(e) {
  effect.setSize(window.innerWidth, window.innerHeight);
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
}

// Get the HMD, and if we're dealing with something that specifies
// stageParameters, rearrange the scene.
function setupStage() {
  navigator.getVRDisplays().then(function(displays) {
    if (displays.length > 0) {
      vrDisplay = displays[0];
      if (vrDisplay.stageParameters) {
        setStageDimensions(vrDisplay.stageParameters);
      }
      vrDisplay.requestAnimationFrame(animate);
    }
  });
}

function setStageDimensions(stage) {
  // Make the skybox fit the stage.
  //var material = skybox.material;
  //scene.remove(skybox);

  // Size the skybox according to the size of the actual stage.
  //var geometry = new THREE.BoxGeometry(stage.sizeX, boxSize, stage.sizeZ);
  //skybox = new THREE.Mesh(geometry, material);

  // Place it on the floor.
  //skybox.position.y = boxSize/2;
  //scene.add(skybox);

  // Place the cube in the middle of the scene, at user height.
  //cube.position.set(0, VRcontrols.userHeight, 0);
}
var dummy = new THREE.Object3D()
window.addEventListener('load', onLoad);
// Mouse Listeners
document.addEventListener('mousemove', onDocumentMouseMove, false );
document.addEventListener('click', onMouseClick);
window.addEventListener( 'mousedown', function () {
	if (datControls.clickMode == 'paint') {
		TransformControls.attach(dummy);
		TransformControls.visible = false;
		datControls.brushDown = true;

	}
}, false );
window.addEventListener( 'mouseup', function() {
	if (datControls.clickMode == 'paint') {
		datControls.brushDown = false;
	}

	// MARK MODE APPLY DECAL
	if ( 0 <= cursorPosition[0] && cursorPosition[0] <= CANVAS_WIDTH &&
			 0 <= cursorPosition[1] && cursorPosition[1] <= CANVAS_HEIGHT) {
				 // Shoot only within the canvas.
					rayShoot();
					if (intersection.intersects && (datControls.clickMode == 'mark')) {
						if (DEBUG) { console.log('--- APPLY DECAL --- Mark Click Mode')}
						applyDecal(selectedObject);
					};
	}
});
window.addEventListener( 'mousemove', onTouchMove );
window.addEventListener( 'touchmove', onTouchMove );

function onTouchMove( event ) {
	if ( 0 <= cursorPosition[0] && cursorPosition[0] <= CANVAS_WIDTH &&
			 0 <= cursorPosition[1] && cursorPosition[1] <= CANVAS_HEIGHT) {
				 // Shoot only within the canvas.
				 rayShoot();
			 }

};

// STATSfunction
function initStats() {
    // Display the Stats Module for performance visualization
    stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms
		stats.domElement.style.left = '20px';
    stats.domElement.style.top = '74px';
    if (DEBUG) {
      document.getElementById("stats-div").appendChild(stats.domElement);
    }

    return stats;
}


datControls = new function() {
    // Implements the logic for the DAT.GUI Interface

    /* CONTROL PARAMS */
		this.Object = '';
		this.currentCamera = 0;
		/* Transform MENU */
		this.Operation = 'translate'
		this.X = 1;
		this.Y = 1;
		this.Z = 1;
		this.helperSize = 3;
		/* DECALS */
		this.markSize = 1;
		this.decalInfoPath = '';
		this.decalFileName = '';
		this.markColor = "#ff0000";
		this.selectedMark = '';
		this.clickMode = 'orbit'
		this.brushDown = false;


		/*CONTROL FUNCS */

		// DEBUG
    this.logSelectedObject = function () {
      console.log(selectedObject);
    };

		// Reload Page clicking a button
		this.Restart = function () {
			if (DEBUG) { console.log(' --- RESTARTING ---')}
			location.reload()
		};

		// Refresh all fields
		this.updateFields = function (aGUI, recursive=false) {
				if (false) { console.log('UPDATING FIELDS'); };
				for (var i in aGUI.__controllers) {
						aGUI.__controllers[i].updateDisplay();

					if (recursive) {
						for (var j in aGUI.__folders){
							datControls.updateFields(aGUI.__folders[j], true);
						};
					};
				};
		};

		this.applyHelperSize = function () {

			TransformControls.setMode(datControls.Operation);
			TransformControls.setSize(datControls.helperSize);
			TransformControls.update();
			if (DEBUG) {console.log(TransformControls.size)}
		}

		this.toggleHelper = function() {
			TransformControls.enabled = true;
			TransformControls.visible = !TransformControls.visible;
		}

		/* CLICKING MODES */

		this.Orbit = function () {
			orbitControls.enabled = !orbitControls.enabled;

			dragControls.enabled = false;
			line.visible = false;
			if (orbitControls.enabled) {
				this.clickMode = 'orbit'
			}
			if (DEBUG) { console.log(this.clickMode)};
		};
		this.Dragging = function () {
			dragControls.enabled = !dragControls.enabled;
			orbitControls.enabled = false;
			line.visible = false;
			if (dragControls.enabled) {
				this.clickMode = 'drag'
			}
			if (DEBUG) { console.log(this.clickMode)};
		};
		this.Marking = function () {
			line.visible = true;
			this.clickMode = 'mark'
			TransformControls.enabled = false;

			if (DEBUG) { console.log(this.clickMode)};
		};
		this.Painting = function () {
			this.clickMode = 'paint'
			line.visible = true;
			orbitControls.enabled = false;
			dragControls.enabled = false;
			TransformControls.enabled = false;
			TransformControls.attach(dummy);
			TransformControls.visible = false;



			if (DEBUG) { console.log(this.clickMode)};
		};

		this.OrbitMode = function () {
			datControls.currentCamera = 'orbit';
		}

		this.VRMode = function () {
			document.getElementById('magic-window').click();
		}



};

// init DatGui
function initDatGui() {
	// DAT GUI CONTROLS
	gui = new dat.GUI({ autoPlace: false });
	gui.domElement.id = 'gui';
	//document.getElementById('controls-div').appendChild(gui.domElement);

	if (DEBUG) {
	var debugGui = gui.addFolder('DEBUG GUI');
	debugGui.add(datControls, 'logSelectedObject');
	}


	var selectionGui = gui.addFolder('Current Object');
	selectionGui.add(datControls, 'Object');

	var transformGui = gui.addFolder('Transform');
	transformGui.add(datControls, 'Operation', {Translate: 'translate',
																							Rotate: 'rotate',
																							Scale: 'scale'});
	transformGui.add(datControls, 'helperSize', 0.1, 1.5);
	transformGui.add(datControls, 'applyHelperSize');
	transformGui.add(datControls, 'toggleHelper');

  /*
	transformGui.add(datControls, 'X');
	transformGui.add(datControls, 'Y');
	transformGui.add(datControls, 'Z');
	*/

	var cameraGui = gui.addFolder('Camera');
	cameraGui.add(datControls, 'OrbitMode');
	cameraGui.add(datControls, 'VRMode');
	gui.add(datControls, 'Restart');


	gui.domElement.addEventListener('change', function() {
		datControls.updateFields(gui, true);
  });
  return gui
};

gui = initDatGui();
$( function() {
	$( "#controls-div" ).draggable();
} );


function rayShoot() {
  if (DEBUG) {console.log('--- Ray Shoot ---')}
  /* RAY CASTER OPERATIONS */

  // update the picking ray with the camera and mouse position
  raycaster.setFromCamera(mouse, currentCamera);

  // calculate objects intersecting the picking ray
  var intersects = raycaster.intersectObjects(objects, true);

  // Perform Operations with intersected objects.
  if ( intersects.length > 0 ) {

		if (DEBUG) { console.log('-- OBJECT(S) INTERSECTED') };

      if (  intersects[0].object.tag != 'grid'   &&
						intersects[0].object.tag !=  'stage' &&
						intersects[0].object.tag !=  'axes'  &&
						intersects[0].object.tag != 'decal') {

					intersectedType = 'object';

          INTERSECTED = intersects[0].object;
          INTERSECTEDPROPS = intersects[0];

          // Check for Decals
          if (intersects[0].face != null) {
            var p = intersects[0].point;
            mouseHelper.position.copy( p );
            intersection.point.copy( p );
            var n = intersects[0].face.normal.clone();
            n.multiplyScalar( 5 );
            n.add( intersects[0].point );
            intersection.normal.copy( intersects[0].face.normal );
            mouseHelper.lookAt( n );
  					line.geometry.vertices[ 0 ].copy( intersection.point );
  					line.geometry.vertices[ 1 ].copy( n );
            line.geometry.verticesNeedUpdate = true;
            intersection.intersects = true;
          }

					if (DEBUG) { console.log('--- not decal intersected ---') };

      } else if (intersects[0].object.tag == 'decal') {

				if (DEBUG) { console.log('-- DECAL INTERSECTED') };
				if (DEBUG) {console.log(intersects[0].object.id) };

				// If intersected a decal, keep the same object as current objected
				// selected.
				intersectedType = 'decal';
				INTERSECTED = intersects[0].object;
				INTERSECTEDPROPS = intersects[0];
			}

			// Apply decal on intersect only within the canvas,
			// when in paint mode, and when the brush is down
      if (0 <= cursorPosition[0]  && cursorPosition[0] <= CANVAS_WIDTH  &&
          0 <= cursorPosition[1]  && cursorPosition[1] <= CANVAS_HEIGHT &&
          intersection.intersects && (datControls.clickMode == 'paint')     &&
					datControls.brushDown) {

              applyDecal(selectedObject);

           };

  } else {

			INTERSECTED = null;
			INTERSECTEDPROPS = null;
			if (DEBUG) { console.log('-- NO OBJECT(S) INTERSECTED') };
        //selectedObject = null;
				//selectedDecal = null;
        intersection.intersects = false;
  };
};

function applyDecal(selectedOBJ) {
			decalPosition.copy( intersection.point );
			decalOrientation.copy(mouseHelper.rotation);
			decalSize.set( datControls.markSize, datControls.markSize, datControls.markSize );

      if (DEBUG) {console.log('Decal Size: ' + decalSize.x + ' ' + decalSize.y + ' ' + decalSize.z )}

      var test_mat = new THREE.MeshLambertMaterial({
        color: cssToHex(datControls.markColor),
        emissive : 0x000000,
        polygonOffset: true,
        polygonOffsetFactor: -1.0,
        polygonOffsetUnits: -4.0
      });


		  var mark = new THREE.Mesh( new THREE.DecalGeometry(
								selectedObject, decalPosition, decalOrientation, decalSize),
					 			test_mat);

			// adjust to position
			mark.position.x -= selectedOBJ.position.x
			mark.position.y -= selectedOBJ.position.y
			mark.position.z -= selectedOBJ.position.z

			// Tag it
			mark.tag = 'decal';

			// Associate with object (NOT Children)
			selectedOBJ.decalinfo.push([decalPosition.toArray(), decalOrientation.toArray(), decalSize.toArray(), test_mat.toJSON()]);
      selectedOBJ.decals.push(mark);

			// Make Intersectable
      objects.push(mark);

			// Add to Scene
			scene.add(mark);


      datControls.updateFields(gui, true);

			if (DEBUG) { console.log('--- ADDED MARK ---'); console.log(mark) };

};

function onDocumentMouseMove(event) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
    event.preventDefault();
		cursorPosition[0] = event.clientX;
		cursorPosition[1] = event.clientY;
    mouse.x = ( event.clientX / CANVAS_WIDTH  ) * 2 - 1;
    mouse.y = - ( event.clientY / CANVAS_HEIGHT ) * 2 + 1;
		if (false) { console.log('mouse.x: ' + mouse.x + ', mouse.y: ' + mouse.y)};
}

function getCursorPosition(canvas, event) {
	  //var rect = canvas.getBoundingClientRect();
    //var x = event.clientX - rect.left;
    //var y = event.clientY - rect.top;
    return [event.clientX, event.clientY];
}

function onMouseClick() {
	if (DEBUG) {
				console.log('on Mouse click');
				console.log(cursorPosition);
				console.log(CANVAS_WIDTH);
				console.log(CANVAS_HEIGHT);
	};

  if (INTERSECTED != null               &&
		  0 <= cursorPosition[0]            &&
      cursorPosition[0] <= CANVAS_WIDTH &&
			0 <= cursorPosition[1]            &&
			cursorPosition[1] <= CANVAS_HEIGHT) {
		if (DEBUG) { console.log('--- valid click!---')}
				switch(intersectedType) {
				    case 'object':
				        selectedObject = INTERSECTED;
								TransformControls.attach(selectedObject);
								TransformControls.visible = false;
								datControls.Object = selectedObject.name;
				        break;
				    case 'decal':
				        selectedDecal = INTERSECTED;
								datControls.selectedDecal = selectedDecal.uuid;
				        break;
				    default:
				        break;
				}

      datControls.updateFields(gui, true);
    }

/*
    if (datControls.viewMode == 'mark' && INTERSECTED != null && INTERSECTED == selectedObject){
      clickPoint = INTERSECTEDPROPS;
    }
*/
};
