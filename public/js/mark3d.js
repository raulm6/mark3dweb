/* mark3D
*  Developer: Raul Monraz
*
 */

/* GLOBAL VARS */
var DEBUG
var initialPositions = {x: 0, y: 0, z: 0};
var initialLookAt = new THREE.Vector3( 0, 0, 10 );
var up = new THREE.Vector3( 0, 1, 0 );
var Xaxis = new THREE.Vector3(1,0,0);
var Yaxis = new THREE.Vector3(0,1,0);
var Zaxis = new THREE.Vector3(0,0,1);

// WebGL DOM Element
var CANVAS_WIDTH;
var CANVAS_HEIGHT;
var container = document.getElementById('WebGL-output');

// ThreeJS Rendering
var camera;
var scene;
var renderer;
var ASPECTRATIO = .75;

// UI
var gui, controls, orbitControls, dragControls, axisHelper, gridHelper, stage;
var moveableLights = [];
var availableCameras = [];

// Object Seelction
var mouse = new THREE.Vector2();
var cursorPosition;

// Ray Casting
var raycaster = new THREE.Raycaster();
var INTERSECTED;
var INTERSECTEDPROPS;
var clickPoint = null;
var intersectedType = null;
// objects checked by raycaster
var objects = [];
// Currently selected object
var selectedObject = null;

// Decals
// Currently Selected Decal
var selectedDecal = null;
var mouseHelper;
var intersection = {
			intersects: false,
			point: new THREE.Vector3(),
			normal: new THREE.Vector3()
};
var decalMaterial;
var decalDiffuse;
var decalNormal;
var line, lineGeometry;
var decalPosition = new THREE.Vector3();
var decalOrientation = new THREE.Euler();
var decalSize = new THREE.Vector3( 10, 10, 10 );

// UV Mapping

/* ------- */


// ThreeJS Initialization
function init(DEBUGPAR=false) {

    DEBUG = DEBUGPAR;
    if (DEBUG) { console.log('---- DEBUG = true') };

    /* WebGL Rendering Canvas Set-up */
    var dimensions = container.getBoundingClientRect();
    CANVAS_WIDTH = dimensions.width;
    CANVAS_HEIGHT = CANVAS_WIDTH * ASPECTRATIO;

    /* RENDERER */
    renderer = new THREE.WebGLRenderer({ antialias: true,
																				 alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setClearColor( 0xeeeee, 0);
    renderer.setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // Insert the Renderer DOM element into the designated HTML element.
    document.getElementById("WebGL-output").appendChild(renderer.domElement);

    /* PARENT SCENE */
    scene = new THREE.Scene();

    /* SCENE CONTENT ---------- */

    // TEXTURE LOADING
    var textureLoader = new THREE.TextureLoader();
    var textureMark3D = textureLoader.load( "textures/Mark3D.png" );

    /* Decal Demo
		decalDiffuse = textureLoader.load( 'textures/decal/decal-diffuse.png' );
	  decalNormal = textureLoader.load( 'textures/decal/decal-normal.jpg' );
    decalMaterial = new THREE.MeshPhongMaterial( {
      specular: 0x444444,
			map: decalDiffuse,
			normalMap: decalNormal,
			normalScale: new THREE.Vector2( 1, 1 ),
			shininess: 30,
			transparent: true,
			depthTest: true,
			depthWrite: false,
			polygonOffset: true,
			polygonOffsetFactor: -1.0,
      polygonOffsetUnits: -4.0,
			wireframe: false
		} );
		*/

    // Decal Mouse Helper
    mouseHelper = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 5 ), new THREE.MeshNormalMaterial() );
    if (DEBUG) {console.log(mouseHelper);}
		mouseHelper.visible = false;
		scene.add( mouseHelper );

    // Decals Line Cursor
    lineGeometry = new THREE.Geometry();
    lineGeometry.vertices.push( new THREE.Vector3(), new THREE.Vector3() );
    line = new THREE.Line( lineGeometry, new THREE.LineBasicMaterial( { linewidth: 4 } ) );
    line.visible = false;
    scene.add( line );

    // UI Guide AXIS
    axisHelper = new THREE.AxisHelper( 60 );
    axisHelper.tag = 'axes';
    axisHelper.position.y = 0.005;

    scene.add( axisHelper );

    // UI Helper Grid
    var size = 50;
    var divisions = 50;
    gridHelper = new THREE.GridHelper( size, divisions );
		gridHelper.tag = 'grid';
		gridHelper.matrixAutoUpdate  = false;
    scene.add( gridHelper );

    // Scene Stage
    var planeGeometry = new THREE.PlaneGeometry(50, 50, 1, 1);
    var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff, map: textureMark3D});
    planeMaterial.side = THREE.DoubleSide;

    stage = new THREE.Mesh(planeGeometry, planeMaterial);
    stage.receiveShadow = true;
    stage.tag = 'stage';
		stage.visible = false;


    // rotate and position the plane
    stage.rotation.x = -0.5 * Math.PI;

		/*
    stage.position.x = 0;
    stage.position.y = 0;
    stage.position.z = 0;
		*/

		scene.add(stage);

		/* SKY
		var skyGeo = new THREE.SphereGeometry(100000, 25, 25);
		/*
		var texture = THREE.ImageUtils.loadTexture( "images/space.jpg" );
		var material = new THREE.MeshPhongMaterial({
			 map: texture,
		});

		var material = new THREE.MeshPhongMaterial({color: 0xa7b6ce });


		var sky = new THREE.Mesh(skyGeo, material);
    sky.material.side = THREE.BackSide;
    scene.add(sky);
		*/

    /* LIGHTS */
    // ambient lighting
    scene.add( new THREE.AmbientLight( 0x505050 ) )

    // add spotlight for the shadows
    var spotLight = new THREE.SpotLight(0xffffff);
    spotLight.position.set(-40, 60, -10);
    spotLight.castShadow = true;
    spotLight.shadow.mapSize.height = 2048;
    spotLight.shadow.mapSize.width = 2048;
    scene.add(spotLight);
		moveableLights.push(spotLight);

    /* CAMERAS */
    camera = new THREE.PerspectiveCamera(45, CANVAS_WIDTH / CANVAS_HEIGHT, 0.5, 1000);
    // Position and point the camera to the center of the scene
    camera.position.x = 10;
    camera.position.y = 10;
    camera.position.z = 10;
    camera.lookAt(scene.position);
		availableCameras.push(camera);


    /* INTERFACES */

    // STATS
    stats = initStats();

    // DAT.GUI
    gui = initDatGui()
    gui.domElement.id = 'gui';

    // Insert the DAT.GUI Interface into the HTML
    document.getElementById("controls-div").appendChild(gui.domElement);

			/* OrbitControls - PAN, ZOOM, ORBIT */
			orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
			orbitControls.enabled = true;


    /* Decals Implementation */
    orbitControls.addEventListener( 'change', function() {

		});

		window.addEventListener( 'mousedown', function () {
      if (controls.viewMode == 'paint') {
        controls.brushDown = true;
      }
		}, false );

		window.addEventListener( 'mouseup', function() {
      if (controls.viewMode == 'paint') {
        controls.brushDown = false;
      }

      if (DEBUG) {
        console.log('mouseup!')
        console.log('cursorPosition x: ' + cursorPosition[0] + 'y: ' + cursorPosition[1]) ;
        console.log('CANVAS_WIDTH: ' + CANVAS_WIDTH + 'CANVAS_HEIGHT ' + CANVAS_HEIGHT);
        console.log(controls.viewMode);
      }

      if ( 0 <= cursorPosition[0] && cursorPosition[0] <= CANVAS_WIDTH &&
           0 <= cursorPosition[1] && cursorPosition[1] <= CANVAS_HEIGHT) {
             // Shoot only within the canvas.
              rayShoot();
							if (intersection.intersects && (controls.viewMode == 'mark')) {
								if (DEBUG) { console.log('--- APPLY DECAL ---')}
								applyDecal(selectedObject);
							};
      }
		});

		window.addEventListener( 'mousemove', onTouchMove );
		window.addEventListener( 'touchmove', onTouchMove );

		function onTouchMove( event ) {
      if ( 0 <= cursorPosition[0] && cursorPosition[0] <= CANVAS_WIDTH &&
           0 <= cursorPosition[1] && cursorPosition[1] <= CANVAS_HEIGHT) {
             // Shoot only within the canvas.
             rayShoot();
           }

		};

    // DragControls  - Drag Objects
    dragControls = new THREE.DragControls(objects, camera, renderer.domElement );
    dragControls.enabled = false


    dragControls.addEventListener('dragstart',
      function (event) {
        if (dragControls.enabled) {
            orbitControls.enabled = false;
        }
      }
    );

    dragControls.addEventListener('dragend',
      function (event) {
        if (dragControls.enabled) {
          orbitControls.enabled = true;
        }
      }
    );


    /* DEBUG CONTENT */

    if (DEBUG) {
	    objLoader = new THREE.OBJLoader();

	    objLoader.load(
	        'models/monkey.obj',
	        function(obj) {
	            //add the loaded object to the scene
	            obj.position.x = 0;
	            obj.position.y = 0;
	            obj.position.z = 0;
	            obj.lookAt(initialLookAt);
	            obj.name = 'Monkey' + '-Parent-' + obj.id ;

	            // CREATE THE INITIAL COLORS
	            obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
	            if (obj.geometry) {
	              var directGeo = new THREE.Geometry();
	              directGeo.fromBufferGeometry(obj.geometry);
	              obj.geometry = directGeo
	            }

	            // Add Children
	            for (var i = 0; i < obj.children.length; i++) {

	                obj.children[i].name = 'Monkey' + '-Child-' + i + '-' + obj.id;

	                obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});

	                obj.children[i].position.x = initialPositions.x;
	                obj.children[i].position.y = initialPositions.y;
	                obj.children[i].position.z = initialPositions.z;
	                obj.children[i].lookAt(initialLookAt);

									// Convert BufferGeometry to Regular Geometry for manipulation
	                if (obj.children[i].geometry) {
	                  var directGeo = new THREE.Geometry();
	                  directGeo.fromBufferGeometry(obj.children[i].geometry);
	                  obj.children[i].geometry = directGeo
	                }

									// Consider the object for intersection
	                objects.push(obj.children[i]);

	                // Initialize Decals Array
									obj.children[i].decalinfo = [];
	                obj.children[i].decals = [];
	                scene.add(obj.children[i]);

	            };

	            console.log('--- LOADED MONKEY DEBUG ---');

	            //scene.add(obj); 'Don't Add Parent'

	        },

	        // Function called when download progresses
	        function ( xhr ) {
	          if (DEBUG) { console.log( (xhr.loaded / xhr.total * 100) + '% loaded' ); }
	        },

	        // Function called when download errors
	        function ( xhr ) {
	          if (DEBUG) { console.error( 'An error happened while loading an .OBJ Model from: ' + path  ); }
	        }
	    );
  }

    /* BEGIN RENDERING */
		onResize();
    // Animation Step
    var step = 0;
    render();
}

/* Helper Functions */
controls = new function() {
    // Implements the logic for the DAT.GUI Interface

    /* CONTROL PARAMS */

    // Path to object to upload
    this.objPath = "";
    // Object to upload file name
    this.fileName = "";
    this.name = "";
    // Position Params
    this.x = 0;
    this.y = 0;
    this.z = 0;
    // Scale Params
    this.sx = 1;
    this.sy = 1;
    this.sz = 1;
    // Rotation Params
    this.rotX = 0;
    this.rotY = 0;
    this.rotZ = 0;
    // Rotation Sum Params
    this.total_rotX = 0;
    this.total_rotY = 0;
    this.total_rotZ = 0;
    // View Modes
    this.viewMode = 'view';
    // Decal Params
    this.decalScale = 0.5;
		this.decalInfoPath = '';
		this.decalFileName = '';
		this.decalColor = "#ff0000";
		this.selectedDecal = '';
    // Painting Params
    this.brushDown = false;
		// JSON Exporting
		this.JSONpath = '';
		this.JSONfileName = '';

    this.uploadOBJ = function () {
      // upload a user specified object
      var extension = controls.fileName.slice(controls.fileName.length - 3,controls.fileName.length)
      var objLoader;
      if (extension == 'obj') { // OBJ Loader
          objLoader = new THREE.OBJLoader();

          objLoader.load(
              this.objPath,
              function(obj) {
                  //add the loaded object to the scene
                  obj.position.x = 0;
                  obj.position.y = 0;
                  obj.position.z = 0;
                  obj.lookAt(initialLookAt);
                  obj.name = controls.fileName + '-Parent-' + obj.id ;

                  // CREATE THE INITIAL COLORS
                  obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
                  if (obj.geometry) {
                    var directGeo = new THREE.Geometry();
                    directGeo.fromBufferGeometry(obj.geometry);
                    obj.geometry = directGeo
                  }

                  // Add Children
                  for (var i = 0; i < obj.children.length; i++) {
                      obj.children[i].name = controls.fileName + '-Child-' + i + '-' + obj.id;
                      obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
                      obj.children[i].position.x = initialPositions.x;
                      obj.children[i].position.y = initialPositions.y;
                      obj.children[i].position.z = initialPositions.z;
                      if (obj.children[i].geometry) {
                        var directGeo = new THREE.Geometry();
                        directGeo.fromBufferGeometry(obj.children[i].geometry);
                        obj.children[i].geometry = directGeo
                      }
                      obj.children[i].lookAt(initialLookAt);
                      objects.push(obj.children[i]);

											// add decals Array
			                obj.children[i].decals = [];
											obj.children[i].decalinfo = [];

                      scene.add(obj.children[i]);
                  };
									objects.push(obj);
                  //scene.add(obj);
              },

              // Function called when download progresses
              function ( xhr ) {
                if (DEBUG) { console.log( (xhr.loaded / xhr.total * 100) + '% loaded' ); }
              },

              // Function called when download errors
              function ( xhr ) {
                if (DEBUG) { console.error( 'An error happened while loading an .OBJ Model from: ' + path  ); }
              }
          );

      } else if (extension == 'dae') { // Collada Loader
        if (DEBUG) { console.log( '-- Collada Loader --' ); }
          objLoader = new THREE.ColladaLoader();
          objLoader.options.convertUpAxis = true;
          objLoader.load( this.objPath, function ( collada ) {
            dae = collada.scene;
            dae.traverse( function ( child ) {
              if ( child instanceof THREE.Mesh ) {
                objects.push(child);
              }
            } );
            scene.add(dae);
           });
      }
    };

    this.selectOBJ = function() {
      if (DEBUG) { console.log( '-- Select Upload Object --' ); }
        var input = document.getElementById('obj-path');
        input.addEventListener('change', function () {
            var file = input.files[0];


            var reader  = new FileReader();

            reader.addEventListener("load", function () {
                controls.objPath = reader.result;
                controls.fileName = file.name;
                controls.updateFields(gui, true);
            }, false);
            reader.readAsDataURL(file);
        });
        input.click();
    };


    this.removeObject = function() {
      recursiveRemove(scene, selectedObject.id);
    }


    this.applyRelocation = function () {
      if (DEBUG) { console.log( ' RELOCATE ' ); }
      selectedObject.position.x = this.x;
      selectedObject.position.y = this.y;
      selectedObject.position.z = this.z;
    }

    this.applyScale = function () {
      if (DEBUG) { console.log('SCALE'); }
      selectedObject.scale.x = this.sx;
      selectedObject.scale.y = this.sy;
      selectedObject.scale.z = this.sz;

			selectedObject.updateMatrix();
			selectedObject.updateMatrixWorld();

    }

    this.applyRotation = function () {

      if (DEBUG) { console.log('ROTATE'); }
      var radX = this.rotX * (Math.PI / 180.0)
      var radY = this.rotY * (Math.PI / 180.0)
      var radZ = this.rotZ * (Math.PI / 180.0)

      selectedObject.rotateX(radX);
      selectedObject.rotateY(radY);
      selectedObject.rotateZ(radZ);


      this.total_rotX += radX;
      this.total_rotY += radY;
      this.total_rotZ += radZ;
    }

    this.resetPosition = function () {

      if (DEBUG) { console.log('-- RESET POSITION --');}

      selectedObject.position.x = initialPositions.x;
      selectedObject.position.y = initialPositions.y;
      selectedObject.position.z = initialPositions.z;
      this.x = initialPositions.x;
      this.y = initialPositions.y;
      this.z = initialPositions.z;
      selectedObject.scale.x = 1;
      selectedObject.scale.y = 1;
      selectedObject.scale.z = 1;
      this.sx = 1;
      this.sy = 1;
      this.sz = 1;
      selectedObject.lookAt(initialLookAt);

    };

    this.exportDecals = function() {

        if (DEBUG) { console.log('--- EXPORT DECALS ---'); }

        var objExporter = new THREE.OBJExporter;

        var decalsObj = new THREE.Scene();
        decalsObj.children = selectedObject.decals;

        var result = objExporter.parse(decalsObj);
        var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
        saveAs(blob,controls.name + ' decals' +  ".obj");

    }

    this.exportOBJ = function() {

      if (DEBUG) { console.log('--- EXPORT OBJECT ---'); }

      controls.fileName = selectedObject.name;
      controls.updateFields(gui, true);
      var objExporter = new THREE.OBJExporter;
      var result = objExporter.parse( selectedObject );
      var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
      saveAs(blob,controls.fileName.slice(0,controls.fileName.length - 4)  + ".obj");

    };


		/* Toggles */

    this.toggleAxes = function () {
      axisHelper.visible = !(axisHelper.visible);
    }
    this.toggleGrid = function () {
      gridHelper.visible = !(gridHelper.visible);
    }
		this.toggleStage = function () {
			stage.visible = !(stage.visible);
		}

    this.toggleViewMode = function () {
      this.viewMode = 'view';
      line.visible = false;
      orbitControls.enabled = true;
      if (DEBUG) { console.log('TOGGLE MARK MODE'); console.log('viewMode: ' + this.viewMode);}
    };

    this.toggleMarkMode = function () {
      this.viewMode = 'mark';
      line.visible = true;
      orbitControls.enabled = true;
      if (DEBUG) { console.log('TOGGLE MARK MODE'); console.log('viewMode: ' + this.viewMode);}
    };

    this.togglePaintMode = function () {
      controls.updateFields(gui, true);
      this.viewMode = 'paint';
      line.visible = true;
      orbitControls.enabled = false;

      if (DEBUG) { console.log('TOGGLE PAINT MODE'); console.log('viewMode: ' + this.viewMode);}
    };

    this.toggleEraseMode = function () {
      this.viewMode = 'erase';
      line.visible = true;
      if (DEBUG) { console.log('TOGGLE ERASE MODE'); console.log('viewMode: ' + this.viewMode);}
    };

    this.toggleDragControls = function () {
      dragControls.enabled = !dragControls.enabled
      if (DEBUG) { console.log('TOGGLE dragControls'); console.log('viewMode: ' + this.viewMode);}
    };

		/* Debug Console Logs */

    this.logSelectedObject = function () {
      console.log(selectedObject);
    };

		this.logSelectedDecal = function () {
			console.log(selectedDecal);
		}

    this.logScene = function() {
      console.log(scene);
    }

    this.logintersectedProps = function() {
      console.log('Intersected Props');
      console.log(INTERSECTEDPROPS);
    }

		this.logintersected = function() {
      console.log('Intersected ');
      console.log(INTERSECTED);
    }

    this.logclickPoint = function() {
      console.log('clickPoint');
      console.log(clickPoint);
    }


		/*
    this.mtlPath;
    this.mtlfileName;
    this.objMtlLoad; /* = function () {
      var mtlLoader = new THREE.MaterialLoader();
      //mtlLoader.setPath( 'obj/male02/' );
      mtlLoader.load( this.mtlPath, function( materials ) {
        materials.preload();
        var objLoader = new THREE.OBJLoader();
        objLoader.setMaterials( materials );
        //objLoader.setPath( 'obj/male02/' );
        objLoader.load(
            this.objPath,
            function(obj) {
                obj.position.x = 0;
                obj.position.y = 10;
                obj.position.z = 0;
                obj.lookAt(initialLookAt);
                obj.name = controls.fileName + '-Parent-' + obj.id ;
                //obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000});
                obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
                //vertexColors: THREE.vertexColors});
                //obj.material.emissive.setHex(0x000000);
                //Add Children
                for (var i = 0; i < obj.children.length; i++) {
                    obj.children[i].name = controls.fileName + '-Child-' + i + '-' + obj.id;
                    //obj.children[i].material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
                    obj.material = new THREE.MeshLambertMaterial({color: 0xffffff, emissive : 0x000000,});
                    //vertexColors: THREE.vertexColors});
                    obj.children[i].position.x = initialPositions.x;
                    obj.children[i].position.y = initialPositions.y;
                    obj.children[i].position.z = initialPositions.z;
                    obj.children[i].lookAt(initialLookAt);
                    objects.push(obj.children[i]);
                };
                scene.add(obj);
         });
      });
    };

    this.selectMtlUpload = function() {
        var input = document.getElementById('mtl-path');
        input.addEventListener('change', function () {
            var file = input.files[0];
            var reader  = new FileReader();
            reader.addEventListener("load", function () {
                controls.mtlPath = reader.result;
                controls.mtlfileName = file.name;
                controls.updateFields(gui, true);
            }, false);
            reader.readAsDataURL(file);
        });
        input.click();
    };

		*/

    this.removeSelectedObjDecals = function() {
			selectedObject.decals.forEach( function( d ) {
				scene.remove( d );
			});
			selectedObject.decals = [];
		};

		this.removeAllDecals = function() {
			for (var i = 0; i < scene.children.length; i++) {
				try {
					if (scene.children[i].tag != null && scene.children[i].tag == 'decal') {
						scene.remove(scene.children[i]);
					}
				} catch (e) {
					console.log('children does not have a geometry')
					console.log(e)
				}
			}
		};


		this.loadDecalInfo =  function() {
		      if (DEBUG) { console.log( '-- Loading decal info Object --' ); }
		        var input = document.getElementById('JSON-path');
		        input.addEventListener('change', function () {
		            var file = input.files[0];
		            var reader  = new FileReader();
		            reader.addEventListener("load", function () {
		                controls.decalInfoPath = reader.result;

										var decalinfo = JSON.parse(reader.result)
										if (DEBUG) {
										console.log(decalinfo[0][0]);
										console.log(decalinfo[0][1]);
										console.log(decalinfo[0][2]);
										console.log(decalinfo[0][3]);
										}

										for (var i = 0; i < decalinfo.length; i++) {

											var position, orientation, size;
											position = new THREE.Vector3();
											orientation = new THREE.Euler();
											size = new THREE.Vector3();

											// instantiate a loader
											var loader = new THREE.MaterialLoader();

											var material = loader.parse(decalinfo[i][3]);
											material.emissive = 0x000000;
											material.polygonOffset = true;
											material.polygonOffsetFactor = -1.0;
											material.polygonOffsetUnits = -4.0;

											// Create the Decal Geometry
											var m = new THREE.Mesh(
												new THREE.DecalGeometry(
													selectedObject, position.fromArray(decalinfo[i][0]), orientation.fromArray(decalinfo[i][1]),
													size.fromArray(decalinfo[i][2])),
													material);
													 //test_mat);

											// adjust to position
											m.position.x -= selectedObject.position.x
											m.position.y -= selectedObject.position.y
											m.position.z -= selectedObject.position.z

											m.tag = 'decal';
								      selectedObject.add(m);
								      selectedObject.decals.push(m);
								      objects.push(m);
											//scene.add(m);

											if (DEBUG) {console.log('loading tile tile tile')};
										}

		                controls.decalFileName = file.name;
		                controls.updateFields(gui, true);
		            }, false);
								reader.readAsText(file);
		        });
		        input.click();
		};

    this.saveScene = function() {
      if (DEBUG) { console.log(' --- SAVING SCENE ---')}
      var sceneJSON = scene.toJSON();
      saveJASON(sceneJSON, 'saved_scene.JSON');
    };

    this.saveOBJ = function() {
      if (DEBUG) { console.log(' --- SAVING SELECTED OBJ ---')}
      var objJSON = selectedObject.toJSON();
      saveJASON(objJSON, 'saved_object.JSON');
    }

		this.saveDecalsJSON = function() {
      if (DEBUG) { console.log(' --- SAVING DECALS ---')}

			var group = new THREE.Group()

				for (var i = 0; i < selectedObject.decals.length; i++) {
								var geo = new THREE.Geometry();
								geo.fromBufferGeometry(selectedObject.decals[i].geometry);
								var mat = selectedObject.decals[0].material
								var mesh = new THREE.Mesh(geo, mat);
								group.add(mesh);
				}

				var groupJSON = group.toJSON();

		    saveJASON(groupJSON, selectedObject.name + 'decals.JSON');
    };

		this.saveDecalInfo = function() {
      if (DEBUG) { console.log(' --- SAVING DECALS INFO ---') }
				var decalJSON = selectedObject.decalinfo;
				var decalinfo = selectedObject.decalinfo;
				console.log(decalinfo[0][0]);
				console.log(decalinfo[0][1]);
				console.log(decalinfo[0][2]);
				console.log(decalinfo[0][3]);
	      saveJASON(decalJSON, selectedObject.name  + 'decalinfo.JSON');
    };

		this.removeSelectedDecal = function () {
			scene.remove(selectedDecal);
		};



		this.selectJSON =  function() {
		      if (DEBUG) { console.log( '-- Select JSON Upload Object --' ); }
		        var input = document.getElementById('JSON-path');
		        input.addEventListener('change', function () {
		            var file = input.files[0];
		            var reader  = new FileReader();
		            reader.addEventListener("load", function () {
		                controls.JSONPath = reader.result;
		                controls.JSONfileName = file.name;
		                controls.updateFields(gui, true);
		            }, false);
		            reader.readAsDataURL(file);
		        });
		        input.click();
		  };


		this.loadJSON = function () {
			// instantiate a loader
			var loader = new THREE.ObjectLoader();

			// load a resource
			loader.load(

				// resource URL
			  this.JSONPath,

				// Function when resource is loaded
				function (obj) {
					if (DEBUG) { console.log('loadedJSON'); console.log(obj); }
					objects.push(obj);
					scene.add( obj);
				}
			);
		};

			// Reload Page clicking a button
			this.Restart = function () {
				if (DEBUG) { console.log(' --- RESTARTING ---')}
				location.reload()
			};

			// Refresh all fields
			this.updateFields = function (aGUI, recursive=false) {

					if (DEBUG) { console.log('UPDATING FIELDS'); };
					for (var i in aGUI.__controllers) {
							aGUI.__controllers[i].updateDisplay();

						if (recursive) {
							for (var j in aGUI.__folders){
								controls.updateFields(aGUI.__folders[j], true);
							};
						};
					};
			};
};


function initDatGui() {
	// DAT GUI CONTROLS
	gui = new dat.GUI({ autoPlace: false });

	if (DEBUG) {
	var debugGui = gui.addFolder('DEBUG GUI');
	debugGui.add(controls, 'logScene');
	debugGui.add(controls, 'logSelectedObject');
	debugGui.add(controls, 'logSelectedDecal');
	debugGui.add(controls, 'logintersected');
	debugGui.add(controls, 'logintersectedProps');
	debugGui.add(controls, 'logclickPoint');
	}

	var sceneGui = gui.addFolder('Environment Configuration');
	sceneGui.add(controls, 'toggleAxes');
	sceneGui.add(controls, 'toggleGrid');
	sceneGui.add(controls, 'toggleStage');

	var selectionGui = gui.addFolder('Selected Object');
	selectionGui.add(controls, 'name');
	selectionGui.add(controls, 'removeObject');

	var clickGui = gui.addFolder('Clicking Modes');
	clickGui.add(controls, 'toggleViewMode');
	clickGui.add(controls, 'toggleDragControls');
	clickGui.add(controls, 'toggleMarkMode');
	clickGui.add(controls, 'togglePaintMode');
	//clickGui.add(controls, 'toggleEraseMode');

	var decalGui = gui.addFolder('Decals Menu');
	decalGui.add(controls, 'selectedDecal');
	decalGui.add(controls, 'removeSelectedDecal');
	decalGui.add(controls, 'decalScale');
	decalGui.addColor(controls, 'decalColor');
	//decalGui.add(controls, 'decalInfoPath');
	//decalGui.add(controls, 'decalFileName');
	decalGui.add(controls, 'loadDecalInfo');
	decalGui.add(controls, 'removeSelectedObjDecals');
	decalGui.add(controls, 'removeAllDecals');

	var positionGui = selectionGui.addFolder('Position');
	positionGui.add(controls, 'x');
	positionGui.add(controls, 'y');
	positionGui.add(controls, 'z');
	positionGui.add(controls, 'resetPosition');
	positionGui.add(controls, 'applyRelocation');

	/*
	var scaleGui = selectionGui.addFolder('Scale');
	scaleGui.add(controls, 'sx');
	scaleGui.add(controls, 'sy');
	scaleGui.add(controls, 'sz');
	scaleGui.add(controls, 'applyScale');


	//var rotateGui = selectionGui.addFolder('Rotate');
	//rotateGui.add(controls, 'rotX');
	//rotateGui.add(controls, 'rotY');
	//rotateGui.add(controls, 'rotZ');
	//rotateGui.add(controls, 'applyRotation');

	*/

	var loaderGui = gui.addFolder('Import Model');
	loaderGui.add(controls, 'fileName');
	//loaderGui.add(controls, 'objPath');
	loaderGui.add(controls, 'uploadOBJ');
	loaderGui.add(controls, 'selectOBJ');
	//loaderGui.add(controls, 'mtlPath');
	//loaderGui.add(controls, 'mtlfileName');
	//loaderGui.add(controls, 'selectMtlUpload');
	//loaderGui.add(controls, 'objMtlLoad');
	//loaderGui.add(controls, 'JSONpath');
	loaderGui.add(controls, 'JSONfileName');
	loaderGui.add(controls, 'loadJSON');
	loaderGui.add(controls, 'selectJSON');
	loaderGui.add(controls, 'loadDecalInfo');


	var saveGui = gui.addFolder('Export JSON ');
	saveGui.add(controls, 'saveScene');
	saveGui.add(controls, 'saveOBJ');
	saveGui.add(controls, 'saveDecalsJSON');
	saveGui.add(controls, 'saveDecalInfo');

	var exporterGui = gui.addFolder('Export OBJ Model');
	exporterGui.add(controls, 'exportOBJ');
	exporterGui.add(controls, 'exportDecals');

	gui.add(controls, 'Restart')

  return gui
}


function initStats() {
    // Display the Stats Module for performance visualization
    var stats = new Stats();
    stats.setMode(0); // 0: fps, 1: ms
    // Align top-left
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '20px';
    stats.domElement.style.top = '74px';

    if (DEBUG) {
      document.getElementById("Stats-output").appendChild(stats.domElement);
    }

    return stats;
}

function applyDecal(obj) {
			decalPosition.copy( intersection.point );
			decalOrientation.copy(mouseHelper.rotation);




      var scale = controls.decalScale;
			decalSize.set( scale, scale, scale );

      if (DEBUG) {console.log('Decal Size: ' + decalSize.x + ' ' + decalSize.y + ' ' + decalSize.z )}

			// var material = decalMaterial.clone();
			// material.color.setHex( Math.random() * 0xffffff );

			//var colorval = ;


      var test_mat = new THREE.MeshLambertMaterial({
        color: cssToHex(controls.decalColor), //0xff0000,
        emissive : 0x000000,
        polygonOffset: true,
        polygonOffsetFactor: -1.0,
        polygonOffsetUnits: -4.0
      });

			console.log(test_mat);



			var m = new THREE.Mesh(
				new THREE.DecalGeometry(
					selectedObject, decalPosition, decalOrientation, decalSize),
					 test_mat);

			// adjust to position
			m.position.x -= obj.position.x
			m.position.y -= obj.position.y
			m.position.z -= obj.position.z


			m.tag = 'decal';
			obj.decalinfo.push([decalPosition.toArray(), decalOrientation.toArray(), decalSize.toArray(), test_mat.toJSON()]);
      //obj.add(m);
      obj.decals.push(m);
      objects.push(m);
			scene.add(m);


      controls.updateFields(gui, true);

			if (DEBUG) { console.log('add decal: '); console.log(m) };

		}


function rayShoot() {
  if (DEBUG) {console.log('--- Ray Shoot ---')}
  /* RAY CASTER OPERATIONS */

  // update the picking ray with the camera and mouse position
  raycaster.setFromCamera(mouse, camera);

  // calculate objects intersecting the picking ray
  var intersects = raycaster.intersectObjects(scene.children, true);

  // Perform Operations with intersected objects.
  if ( intersects.length > 0 ) {

		if (DEBUG) { console.log('-- OBJECT(S) INTERSECTED') };

      if (  intersects[0].object.tag != 'grid'   &&
						intersects[0].object.tag !=  'stage' &&
						intersects[0].object.tag !=  'axes'  &&
						intersects[0].object.tag != 'decal') {

					intersectedType = 'object';

          INTERSECTED = intersects[0].object;
          INTERSECTEDPROPS = intersects[0];

          // Check for Decals
          if (intersects[0].face != null) {
            var p = intersects[0].point;
            mouseHelper.position.copy( p );
            intersection.point.copy( p );
            var n = intersects[0].face.normal.clone();
            n.multiplyScalar( 5 );
            n.add( intersects[0].point );
            intersection.normal.copy( intersects[0].face.normal );
            mouseHelper.lookAt( n );
  					line.geometry.vertices[ 0 ].copy( intersection.point );
  					line.geometry.vertices[ 1 ].copy( n );
            line.geometry.verticesNeedUpdate = true;
            intersection.intersects = true;
          }

					if (DEBUG) { console.log('--- not decal intersected ---') };

      } else if (intersects[0].object.tag == 'decal') {

				if (DEBUG) { console.log('-- DECAL INTERSECTED') };
				if (DEBUG) {console.log(intersects[0].object.id) };

				// If intersected a decal, keep the same object as current objected
				// selected.
				intersectedType = 'decal';
				INTERSECTED = intersects[0].object;
				INTERSECTEDPROPS = intersects[0];
			}

			// Apply decal on intersect only within the canvas,
			// when in paint mode, and when the brush is down
      if (0 <= cursorPosition[0]  && cursorPosition[0] <= CANVAS_WIDTH  &&
          0 <= cursorPosition[1]  && cursorPosition[1] <= CANVAS_HEIGHT &&
          intersection.intersects && (controls.viewMode == 'paint')     &&
					controls.brushDown) {

              applyDecal(selectedObject);

           };

  } else {

			INTERSECTED = null;
			INTERSECTEDPROPS = null;
			if (DEBUG) { console.log('-- NO OBJECT(S) INTERSECTED') };
        selectedObject = null;
				selectedDecal = null;
        intersection.intersects = false;
  };
};

function render() {
	// Starts rendering the Scene, called from within init()

  // Render using requestAnimationFrame
  requestAnimationFrame(render);
  renderer.render(scene, camera);

  // PER FRAME UPDATE
  stats.update();
  orbitControls.update();
  //onResize();
}

function recursiveRemove(objectArray, id) {
    // Remove an object from an object Array with a specific id

    for (var i = 0; i < objectArray.children.length; i++) {
      if (objectArray.children[i].id == id) {
        objectArray.remove(objectArray.children[i]);
        return;
      }
    }
    for (var i = 0; i < objectArray.children.length; i++) {
      if (objectArray.children[i].children.length > 0) {
          recursiveRemove(objectArray.children[i], id);
      }
    }
}

function onResize() {
  var dimensions = container.getBoundingClientRect();
  CANVAS_WIDTH = dimensions.width - 7;
  CANVAS_HEIGHT = CANVAS_WIDTH * (3/4);
  camera.aspect = CANVAS_WIDTH / CANVAS_HEIGHT;
  camera.updateProjectionMatrix();
  renderer.setSize(CANVAS_WIDTH, CANVAS_HEIGHT);
}

function onDocumentMouseMove(event) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
    event.preventDefault();
    var containerDOMrect = container.getBoundingClientRect();
    cursorPosition = getCursorPosition(container, event);
    mouse.x = ( cursorPosition[0] / containerDOMrect.width  ) * 2 - 1;
    mouse.y = - ( cursorPosition[1] / containerDOMrect.height ) * 2 + 1;
}

function getCursorPosition(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return [x, y];
}


/* EVENT LISTENERS */

// Start ThreeJS
window.onload = init(false);
// Resize Canvas
window.addEventListener('resize', onResize, false);
// Mouse Intersection
document.addEventListener('mousemove', onDocumentMouseMove, false );
// Mouse Click
document.addEventListener('click', function() {

    if (INTERSECTED != null               &&
			  0 <= cursorPosition[0]            &&
        cursorPosition[0] <= CANVAS_WIDTH &&
				0 <= cursorPosition[0]            &&
				cursorPosition[0] <= CANVAS_HEIGHT) {

					switch(intersectedType) {
					    case 'object':
					        selectedObject = INTERSECTED;
									controls.name = selectedObject.name;
					        break;
					    case 'decal':
					        selectedDecal = INTERSECTED;
									controls.selectedDecal = selectedDecal.uuid;
					        break;
					    default:
					        break;
					}

        controls.updateFields(gui, true);



    }

    if (controls.viewMode == 'mark' && INTERSECTED != null && INTERSECTED == selectedObject){
      clickPoint = INTERSECTEDPROPS;
    }
});
