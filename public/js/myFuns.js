var myget_object_by_name = function(objs, name) {
  for (var i = 0; i < objs.length; i++) {
    console.log(i + ' : ' + objs[i].name);
    if (objs[i].name == 'default') {
      return objs[i];
    }
  }
}

function saveJASON (JSONobj, name) {
  var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(JSON.decycle(JSONobj)));
  //var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(JSONobj));

  var dlAnchorElem = document.getElementById('downloadAnchorElem');
  dlAnchorElem.setAttribute("href",     dataStr     );
  dlAnchorElem.setAttribute("download", name);
  dlAnchorElem.click();

}

function grassmanProduct (q1, q2) {

	out = new THREE.Quaternion();
	v1 = new THREE.Vector3(q1.y, q1.z, q1.w)
	v2 = new THREE.Vector3(q2.y, q2.z, q2.w)

	out.x = (q1.x * q2.x - v1.dot(v2));

	cross = new THREE.Vector3();
	cross.crossVectors(v1, v2)
	dots = new THREE.Vector3();
	dots.addVectors(v1.multiplyScalar(q2.x),v2.multiplyScalar(q1.x));

	sum = new THREE.Vector3();
	sum.addVectors(dots, cross);

	out.y = sum.x;
	out.z = sum.y;
	out.w = sum.z;


return out;

}

function cssToHex(cssString) {
  var hex = parseInt('0x' + cssString.slice(1));
  return hex;
}
